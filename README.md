[Download latest Fiat Lux build (0.2.0) ⬇️](https://bitbucket.org/tvlkdesign/tv-fiat-lux/downloads/v.0.2.0.zip)

# tv Fiat Lux ✨

Fiat lux is latin phrase for _"let there be light"_, denoting the moment of creation of all things. In Traveloka context, this plugin will help you create Traveloka UI component in Sketch. Focus more on problem solving and let this plugin have your back in crafting higher fidelity mockup.

## Pixel perfect, everytime

Don't worry about measurements; all the paddings, margins, and font sizes for the basic components is handled in the embedded styles. Learn more about the style guides [here](https://tvlk-ds.firebaseapp.com).

Some of the component even going an extra-miles by taking care text capitalization, resizing rules, etc. It solves basic UI problems so damn good via automated set-up, so you can focus solving bigger challenge!

## Tap into Design System

This plugin is part of the design system toolkit along with _icon-kit_ and the _documentation_. Access any tools right from this plugin.

## How do I get set up?

- Download Fiat Lux plugin
- Double click to install plugins
- That's it! It will be automatically downloaded next time there is an update

## What's included?

- Make button
- Convert iOS to Android artboards and vice-versa
- Make style elevations
- Make carousel layout
- Commit changes
- Link to documentation

## Feedback?

- Slack (at)uid or (at)arie or (at)jeje

## Changelog

#### V.0.2.0

- Refactoring helper code to be more modular
- Dark UI for component make
- Convert iOS to Android design vice-versa by (at)jeje
- Adding document save-checking before writing commit message
